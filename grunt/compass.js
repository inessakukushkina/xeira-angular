module.exports = {
 dist: {
  options: {             
   sassDir: ['app/src/sass'],
   cssDir : ['app/dest/css'],
   environment: 'development' // 'production'
  }
 }
};