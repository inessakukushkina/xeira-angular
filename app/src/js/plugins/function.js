var clickActive = function($clickBlock, $searchBlock){
	$(document).on('click', $clickBlock, function(){
		$(this).closest($searchBlock).find($clickBlock).removeClass('active');
		$(this).addClass('active');
	})
}

var accordeon = function($link){
    $link.on('click', function(){
        var $this = $(this);
        if($this.next('ul').length > 0){
            var $item 		= $this.closest('li'),
            $parentItem = $item.parent(),
            $_click 	= $item.find('ul:first').is(':visible');
            if(!$_click) {
                $parentItem.find('> li ul:visible').slideToggle();
                $item.find('a').removeClass('active');	
                $item.removeClass('active');			
                $parentItem.find('li').removeClass('active');	
                $parentItem.find('a').removeClass('active');
            }				
            $item.find('ul:first').slideToggle();
            $item.find('a').toggleClass('active');
            $item.toggleClass('active');            

            $(document).bind('click', function(e){
                if (!$(e.target).parents().hasClass("active")) {
                    $item.find('ul:first').slideUp();
                    $item.find('a').removeClass('active');
                    $item.removeClass('active');
                }
            });

            return false;
        }
    })   
}

CountGoods = {
	numericValidationString: /^[0-9]+$/
};

function ValidateNumeric(input){
	return CountGoods.numericValidationString.test(input);
}

$.fn.quantityInput = function(){
    $(this).each(function(){
        var $decrement  = $(this).find('.quantity__decrement'),
            $quantity   = $(this).find('.quantity__control'),
            $incriment  = $(this).find('.quantity__increment');           

       if (parseInt($quantity.val()) <= 1) {
            $decrement.prop('disabled', true);
        } else {
            $decrement.prop('disabled', false);
        }

  		$decrement.on('click', function () {
            if (ValidateNumeric($quantity.val())) {
                $number = parseInt($quantity.val());
                $number--;
                if ($number <= 1) {
                    $quantity.val(1);
                	$decrement.prop('disabled', true);
                } else {
                	$quantity.val($number);
                	$decrement.prop('disabled', false);
                }                            
            } else {
            	$quantity.val(1);
             }
            return false;
        });
     	$incriment.on('click', function () {
            if (ValidateNumeric($quantity.val())) {
                $number = parseInt($quantity.val());
                $number++;

                $quantity.val($number);
                $decrement.prop('disabled', false);
            } else {
            	$quantity.val(1);
            }
            return false;
        });
    });
};

var cartCount = function(){
    var $count = parseInt($('.count_cart').html(), 10);
    if(isNaN($count) || $count < 1){
        $('.count_cart').addClass('hidden');
    } else{
        $('.count_cart').removeClass('hidden');
    }
}

$.fn.carous = function (options) {
    return this.each(function () {
        var settings = $.extend({
            speed: 500,
            outerLeft: 15
        }, options);

        var
            $this = $(this),
            current = 0,
            caruC = $this.find('.carous'),
            caruV = caruC.find('.carous-slides'),
            $thumC = caruC.find('.thumbs-container'),
            view = caruV.find('.item'),
            views = view.length,
            thumbs = caruC.find('.carous-thumbnails ul'),
            thumbLi = thumbs.find('li'),
            thumbAc = thumbs.find('li.active'),
            button = caruC.find('.carous-controls button'),
            maxL, wUl  = null,
            wOver = wUl = maxL = null;
            caruV.css({
                'width': view.width() * views
            });

        view.first().show();
        init();

        if(thumbs.width() <= caruC.width()) {
            button.remove();
        }

        function reset() {
            wUl = 0;
            wOver = $thumC.width();
            thumbLi.each(function () {
                wUl += $(this).width() + settings.outerLeft + 100;
            });
            maxL = wUl - wOver;
            thumbs.width(wUl);
        }

        function init() {
            reset();
        }

        $(window).on('resize', function () {
            reset();
        });

        button.on('click', function () {
            var _this = $(this);
            var activE = null;
            if (_this.hasClass('carous-prev')) {
                var curren = thumbLi.filter('.active');
                if (curren.index() <= 0) {
                    return true;
                } else {
                    curren.removeClass('active');
                    var prev = curren.prev();
                    prev.addClass('active');
                }
                activE = prev;
            } else {
                var curren = thumbLi.filter('.active');
                if (curren.index() >= thumbLi.length - 1) {} else {
                    curren.removeClass('active');
                    var next = curren.next();
                    next.addClass('active');
                }
                activE = next;
            }            
            if (activE) {
                var left = -activE.index() * (thumbLi.width() + settings.outerLeft) + $thumC.width() / 2 - thumbLi.width() / 2 - settings.outerLeft / 2;
                if (0 < left) {
                    left = 0;
                } else if (-maxL > left) {
                    left = -maxL;
                }
              view.find('img').attr('src', activE.find('img').attr(
                    'src', _this.find('img').attr('src'),
                    'data-zoom-image', _this.find('img').attr('src')
                ));

                thumbs.stop().animate({
                    left: left
                }, settings.speed);

            }
             view.find('img').attr('src', thumbs.find('li.active').find('img').attr('src'));
        });

        thumbLi.first().addClass('active');

        thumbLi.on('click', function () {
            var _this = $(this);

            thumbLi.removeClass('active');
            _this.addClass('active');

            view.find('img').attr(
                'src', _this.find('img').attr('src'),
                'data-zoom-image', _this.find('img').attr('src')
            );
        });

    });
}

var wishlist = function($_this){
    var elem = {
        data_add: $_this.closest("[data-add]"),
        data_continue: $(elem.data_add).prev("[data-continue]")
    };
    $(document).on('click', '.close', function($_this){
        $(elem.data_add).slideUp();
    })

    $(document).on('click', '.btn__continue', function($_this){
        $(elem.data_add).hide();
        $(elem.data_continue).slideUp();
    })
}

var popover = function($data_popover, $popover_class){
    var flag_lock   = false;
    $(document).on('click', $data_popover, function(){
        var flag_lock       = true;
        var $_this          = $(this);
        var $popover        = $_this.find($popover_class);
        var $popover_top    = $_this.height()/2 - $popover.height()/2 + 'px';


        if($_this.hasClass('active')){
            $popover.slideUp();
            $_this.removeClass('active');
        } else{
            $($popover_class).slideUp();
            $($data_popover).removeClass('active');
            $_this.addClass('active');
            $popover.slideDown(function(){
                flag_lock = false
            });
            $popover.css('top', $popover_top);  
        }
    });

    $(document).bind('click', function(e){
        if (!$(e.target).parents().hasClass("active")) {
            $popover_class.hide();
            $($data_popover).removeClass('active'); 
        }
    });
}

var blockStick = function ($block){
    if($block.length){
        var $stickTop = $block.offset().top;
        if( $(window).scrollTop() > $stickTop ) {             
            $block.addClass('stick');
        }
        $(window).scroll(function(){
            if( $(window).scrollTop() > $stickTop ) {
                $block.addClass('stick');
            }
            else {
                $block.removeClass('stick');
            }
        });
    } else{
    }
}

 var sliderWidth = function(ui){
    var maxVal = $('#slider').slider("option", "max");
    var endPos = $('#slider').slider("option", "value");
    $('[data-slider]').css('width', (endPos * 100)/maxVal + '%');
    $('#slider-count').val(endPos);
}