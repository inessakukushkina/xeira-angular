$(document).ready(function(){
	clickActive($('.sidebar__guide__item__link'), $('.sidebar__guide'));
	
	accordeon($('.shop__list__item__link'));

	$('.custom-form').checkBo({
        checkAllButton : '.check__all', 
        checkAllTarget : '.cart td', 
    });
	$('.chosen-select').chosen();
	$('.chosen-search').remove();

	$('.quantity__block').quantityInput();

	$(document).on('click', '.btn-menu', function(){
		$('.shop__list').slideToggle();
	})

	$(document).on('click', '.btn-search', function(){
		$('.head .form-search').slideToggle();
		$(this).toggleClass('btn-remove');
	});

    $(document).on('click', '.filter__block .sidebar_title', function(){
        $('.filter__block__title').slideToggle();
        $(this).toggleClass('active');
        $('.filter__block .row').slideUp();
    })

    $(document).on('click', '.sidebar .sidebar_title', function(){
        $('.sidebar__list').hide();
        $('.sidebar .sidebar_title').removeClass('active');
        $(this).next('.sidebar__list').show();
        $(this).toggleClass('active');
    })

    $(document).on('click', '.filter__block__title', function(){
        $('.filter__block .row').hide();
        $('.filter__block__title').removeClass('active');

        if($(this).hasClass('active')){
        	$(this).next('.row').hide();
        	$(this).removeClass('active');	
        } else{
        	$(this).next('.row').show();
        	$(this).addClass('active');	
        }				
    });

    cartCount();

    $('.product__picture').zoom({
        magnify: 3
    });

    $('.home-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        autoplay: true
    });

    $('.month-posts__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1
    });

    $('.post__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true
    });

    $('.following_products').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false
          }
        }]
    });
    
    $('.relating_products').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000        
    });

	$('.add-to-cart').on('click', function(){
        var $_cart 			= $('.shopping-cart');
        var $_product_image = $(this).closest('.product__block').find('.product__picture__image');       

        if ($_product_image) {
            var imgclone    = $_product_image
                            .clone()
                            .offset({
                                top: $_product_image.offset().top,
                                left: $_product_image.offset().left
                            })
                            .css({
                                'opacity': '0.5',
                                'position': 'absolute',
                                'height': '150px',
                                'width': '150px',
                                'z-index': '100'
                            })
                            .appendTo($('body'))
                            .animate({
                                'top': $_cart.offset().top + 10,
                                'left': $_cart.offset().left + 10,
                                'width': 75,
                                'height': 75
                            }, 1000, 'easeInOutExpo');

            setTimeout(function () {
                $_cart.effect("bounce", {
                    times: 2
                }, 200);
            }, 1500);

            imgclone.animate({
                'width': 0,
                'height': 0
            }, function () {
                $(this).detach()
            });

            setTimeout(function () {
                var $_count         = $('.count_cart');
                var $_quantity      = $('.quantity__control').val();
                $_count.html(parseInt($_quantity, 10));
                    cartCount();
            }, 2000)
        }
    })

    $('[data-datepicker]').datepicker();

    $('.hasDatepicker').on('focus', function(){
        $(this).parent().addClass('form-date-focus');
    });

    $('.hasDatepicker').on('blur', function(){
         $(this).parent().removeClass('form-date-focus');
    });
    
    $('.product__gallery').carous();

    $(document).on('click', '[data-address]', function(e){
        e.preventDefault();
        $('.address__settings').toggleClass('is-hidden');
        $('.show__address').closest('.row').toggleClass('is-hidden');
    });

    $(document).on('click', '[data-back]', function(){
        $(this).closest('.block__upload').toggleClass('is-hidden');
        $(this).closest('.block__upload').prev().toggleClass('is-hidden');

        $('[data-list].active').last().prev().removeClass('done-success');
        $('[data-list].active').last().removeClass('active');
    });

    $(document).on('click', '[data-forward]', function(){
        $(this).closest('.block__upload').toggleClass('is-hidden');
        $(this).closest('.block__upload').next().toggleClass('is-hidden');

        $('[data-list].active').last().addClass('done-success');
        $('[data-list].active').last().next().addClass('active');
    });

    $(document).on('click', '.icon-close', function(){
        var $_this          = $(this);
        var $_data_continue = $_this.closest('[data-continue]');
        var $_data_add      = $_data_continue.next('[data-add]');

        $_data_continue.hide();
        $_data_add.fadeIn();
    })

    $(document).on('click', '.btn__continue', function(){
        var $_this          = $(this);
        var $_data_add      = $_this.closest('[data-add]');
        var $_data_continue = $_data_add.prev('[data-continue]');

        $_data_add.hide();
        $_data_continue.fadeIn();
    })

    $(document).on('click', '[data-accordeon]', function(){
        var $_this = $(this);        
        if(!$_this.hasClass('active')){
            $('[data-accordeon]').removeClass('active');
            $('[data-accordeon-body]').slideUp();
            $(this).addClass('active');
            $(this).find('[data-accordeon-body]').slideDown();
        }
    })

    if($('[data-accordeon]').hasClass('active')){
       $('[data-accordeon].active').find('[data-accordeon-body]').show()
    };

    popover('[data-popover]', $('[data-toggle-popover]'));

    $('.tags').tagsinput();
    $('.colorpicker').colorpicker();

    $(document).on('click', '[data-remove]', function(){
        $(this).closest($(this).attr('data-remove')).slideUp();
    });

    $(document).on('click', '[data-toggle-block]', function(){
        var $toggle_block   = $(this).attr('data-toggle-block');
        $($toggle_block).slideToggle();
        return false;
    });

    $(document).on('click', '.popover__block', function(){
        $(this).parent().find('.popover__block').removeClass('active');
        $(this).addClass('active');
    })

    $('.color').colorpicker();

    $('#slider').slider({
        max: 400,
        value: 60,
        step: 5,
        slide: function(event, ui) {
            var endPos  = ui.value; 
            $('#slider-count').val(endPos);
            sliderWidth();
        }
    });

    tinymce.init({
        menubar: false,
        statusbar: false,
        selector: "textarea#description",
        theme: "modern",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | link image"
    });

    sliderWidth();

    blockStick($('[data-affix]'));

    $(document).on('click', '[data-gallery]', function(e){
        e.preventDefault();
        var $_this  = $(this);
        var $_src   = $_this.find('img').attr('src');
        $('[data-gallery]').removeClass('active');
        $_this.addClass('active');
        $('[data-artwork]').attr('src', $_src);
    });

    $('[name="billingCountry"]').select2();
    $('[name="billingState"]').select2();
    $('[name="billingCity"]').select2();
});