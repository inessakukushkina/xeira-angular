(function () {
    var justShirtApp = angular.module('justShirtApp', [
        'ngRoute'
    ]);

    justShirtApp.run(['$rootScope', '$location', '$route',
        function ($rootScope, $location, $route) {

        }
    ]);

    justShirtApp.config(['$routeProvider', '$locationProvider'
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'views/home.html',
                    controller: 'HomeCtrl'
                })
                .when('/sign-in', {
                    templateUrl: 'views/sign-in.html',
                    controller: 'SignInCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
                $locationProvider.html5Mode(true);
        }
    ]);
    
}());