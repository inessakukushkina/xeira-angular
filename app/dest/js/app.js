(function () {
    var justShirtApp = angular.module('justShirtApp', [
        'ngRoute'
    ]);

    justShirtApp.config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider
                .when('/', {
                    templateUrl: 'views/home.html',
                    controller: 'HomeCtrl'
                })
                .when('/login', {
                    templateUrl: 'views/sign-in.html',
                    controller: 'SignInCtrl'
                })
                .otherwise({
                    redirectTo: '/'
                });
                $locationProvider.html5Mode(true);
        }
    ]);
    
}());



