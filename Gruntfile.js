module.exports = function(grunt) {

  // configure the tasks
  grunt.initConfig({
   
   concat: {
    options: {
      separator: ' \n'
    },
    dist: {
		src : ['app/src/js/app/*.js'],
		dest: 'app/dest/js/app.js',
		nonull: true
	},
    plugins: {
        src : [ 'app/src/js/plugins/*.js' ],
        dest: 'app/js/plugins.js',
        nonull: true
    }
  },
    
   compass: {
     dist: {
       options: {
         sassDir: 'app/src/sass',
         cssDir: 'app/dest/css',
         config: 'config.rb',
         environment: 'development'
       }
     }
   },
    
    uglify: {
      build: {
        options: {
          mangle: false
        },
        files: {
          'app/js/main.js': [ 'app/js/**/*.js' ]
        }
      }
    },

    watch: {
      css: {
       files: ['app/src/sass/*.scss', 'app/src/sass/**/*.scss'],
       tasks: ['compass']
      },
      scripts: {
       files: ['app/src/js/*.js', 'app/src/js/**/*.js'],
       tasks: ['concat']
      }        
    }
  });

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');


  // measures the time each task takes
	require('time-grunt')(grunt);

	// load grunt configurations
	require('load-grunt-config')(grunt);

	// load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
	require('load-grunt-tasks')(grunt);
	
  // define the tasks
  grunt.registerTask( 'default',  'Watches the project for changes, automatically builds them and runs a server.',  [ 'compass', 'connect', 'concat', 'watch' ] );
};