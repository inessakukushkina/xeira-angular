$(document).ready(function(){
    Dropzone.options.dropFiles = {
        paramName: "file",
        maxFilesize: 10, // MB
        thumbnailWidth:"320",
        acceptedFiles: '.jpg, .ai, .gif, .sdr, .eps, .png, .svg' ,
        init: function() {
            this.on('thumbnail', function(file, dataUrl) {
                $('.dropzone').addClass('is-hidden');
                $('.confirm__upload').toggleClass('is-hidden');
                $('.upload__gallery__picture').attr('src', dataUrl);
                $('.download__list__item.active').addClass('done-success').next().addClass('active');
            })
        }
    };
});